package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Pattern;

public class FileRepository {

    List<Integer> list=new ArrayList<>();
    public List<Integer> getFromFile(String file){
        File f=new File(file);

        try {
            Scanner sc=new Scanner(f);
            while (sc.hasNextInt())
                list.add(sc.nextInt());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return list;
    }
}
